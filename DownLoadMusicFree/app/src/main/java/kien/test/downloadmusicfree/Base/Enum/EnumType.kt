package kien.test.downloadmusicfree.Base.Enum

enum class EnumType(val type: String) {
    VN("Songs"),
    US_UK("SongsUS")
}