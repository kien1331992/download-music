package kien.test.downloadmusicfree.Service

import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.FirebaseDatabase
import kien.test.downloadmusicfree.Model.Song
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import android.text.method.TextKeyListener.clear
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.ValueEventListener


class SongRepository private constructor() {
    companion object {
        @JvmStatic
        fun getInstance(): SongRepository {
            return Holder.INSTANCE
        }

    }

    private object Holder {
        val INSTANCE = SongRepository()
    }

    init {

    }

    public fun getAllSong(completion: (MutableList<Song>) -> Unit, type : String) {
        var arrSongs: MutableList<Song> = mutableListOf()
        var db = FirebaseDatabase.getInstance().getReference(type)
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (data in dataSnapshot.children) {
                    val data: Song? = data.getValue(Song::class.java)
                    if (data != null) {
                        arrSongs.add(data)
                    }
                }
                completion(arrSongs)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w("kienmt", "loadPost:onCancelled", databaseError.toException())
            }
        })
    }
}

