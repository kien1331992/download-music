package kien.test.downloadmusicfree.View

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.material.bottomnavigation.BottomNavigationView
import kien.test.downloadmusicfree.Base.BaseActivity
import kien.test.downloadmusicfree.Base.SharedPrefs
import kien.test.downloadmusicfree.R
import com.google.firebase.iid.FirebaseInstanceId
import android.os.SystemClock
import android.app.*
import android.os.Build
import kien.test.downloadmusicfree.Service.NotificationPub


class MainActivity : BaseActivity() {
    val FRAG_VN = "vn_frag"
    val FRAG_US = "us_frag"

    override fun attachLayout(): Int {
        return R.layout.activity_main
    }

    override fun initView() {
        btlnv = findViewById(R.id.btnv) as BottomNavigationView
        frm_container = findViewById(R.id.btnv) as FrameLayout
        imvTutorial = findViewById(R.id.imvTutorial) as ImageView

        registerReceiver(
            onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )

        checkPref()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val newToken = instanceIdResult.token

        }

        scheduleNotification(getNotification("5 second delay"), 5000);
    }

    private fun scheduleNotification(notification: Notification, delay: Int) {

        val notificationIntent = Intent(this, NotificationPub::class.java)
        notificationIntent.putExtra(NotificationPub.NOTIFICATION_ID, 1)
        notificationIntent.putExtra(NotificationPub.NOTIFICATION, notification)
        val pendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val futureInMillis = SystemClock.elapsedRealtime() + delay
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent)
    }

    private fun getNotification(content: String): Notification {
        var builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, NotificationPub.NOTIFICATION_ID)
        } else {
            Notification.Builder(this)
        }
        builder.setContentTitle("Scheduled Notification");
        builder.setContentText(content);
        builder.setSmallIcon(R.mipmap.ic_icon);
        return builder.build();
    }

    private fun checkPref() {
        if (SharedPrefs.instance.get(SharedPrefs.PREFS_OpenApp, Boolean::class.java)!! == false) {
            SharedPrefs.instance.put(SharedPrefs.PREFS_OpenApp, true)
            showDialog()
        }
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.title_dialog))
        builder.setMessage(getString(R.string.content_dialog))
        builder.setCancelable(true)
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            dialog.dismiss()
        }
        builder.show()
    }

    override fun initEven() {
        btlnv.setOnNavigationItemSelectedListener(onNavigationView)
        imvTutorial.setOnClickListener(View.OnClickListener {
            showDialog()
        })
    }

    override fun initDataServer() {
    }

    private val onNavigationView =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_music -> {
                    loadFragByTag(FRAG_VN)
                }
                R.id.action_us_uk -> {
                    loadFragByTag(FRAG_US)
                }
            }
            true
        }

    lateinit var btlnv: BottomNavigationView
    lateinit var frm_container: FrameLayout
    lateinit var imvTutorial: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadFragByTag(FRAG_VN)
    }

    private fun loadFragByTag(tag: String) {
        var frag = supportFragmentManager.findFragmentByTag(tag)
        if (frag == null) {
            when (tag) {
                FRAG_VN -> {
                    frag = MusicVNFragment()
                }
                FRAG_US -> {
                    frag = MusicUSUKFragment()
                }

            }

        }

        if (frag != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frm_container, frag, tag)
                .commit()
        }

    }

    internal var onComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctxt: Context, intent: Intent) {
            val referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            @SuppressLint("ResourceAsColor") val mBuilder =
                NotificationCompat.Builder(this@MainActivity)
                    .setSmallIcon(R.mipmap.ic_down)
                    .setContentTitle(getString(R.string.title_down))
                    .setContentText(getString(R.string.download_complete))


            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(455, mBuilder.build())
            Toast.makeText(
                this@MainActivity,
                resources.getString(R.string.finish) + getString(R.string.app_name),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    protected override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(onComplete)
    }
}
