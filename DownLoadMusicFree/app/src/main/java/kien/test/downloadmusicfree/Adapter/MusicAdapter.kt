package kien.test.downloadmusicfree.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kien.test.downloadmusicfree.Model.Song
import kien.test.downloadmusicfree.R

public class MusicAdapter(ct: Context, arrSongs: List<Song>, _even: CallBackDownLoad) :
    RecyclerView.Adapter<MusicAdapter.ViewHolder>() {
    private lateinit var context: Context
    private lateinit var arrUser: List<Song>
    private lateinit var even: CallBackDownLoad

    init {
        this.context = ct
        this.arrUser = arrSongs
        this.even = _even
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(this.context).inflate(R.layout.item_song, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrUser.size
    }

    fun setData(newData: List<Song>) {
        this.arrUser = newData
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var song: Song = arrUser.get(position)
        holder.tvNameSong.text = song.nameSong
        holder.tvNameArtis.text = song.nameArtis
//        Glide.with(context).load(song.linkAvatar).placeholder(R.mipmap.ic_placeholder)
//            .into(holder.imvAvatar)
        holder.imvDownload.setOnClickListener(View.OnClickListener {
            even.onClick(song.linkDownLoad, song.nameSong)
        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public lateinit var tvNameSong: TextView
        public lateinit var tvNameArtis: TextView
        public lateinit var imvDownload: ImageView
        public lateinit var imvAvatar: ImageView

        init {
            tvNameSong = itemView.findViewById(R.id.tvNameSong)
            tvNameArtis = itemView.findViewById(R.id.tvNameArtis)
            imvDownload = itemView.findViewById(R.id.imvDownLoad)
            imvAvatar = itemView.findViewById(R.id.imvAvatar)
        }

    }

    interface CallBackDownLoad {
        fun onClick(url: String, name: String)
    }
}