package kien.test.downloadmusicfree.View


import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kien.test.downloadmusicfree.Adapter.MusicAdapter
import kien.test.downloadmusicfree.Base.BaseFragment
import kien.test.downloadmusicfree.Base.Enum.EnumType
import kien.test.downloadmusicfree.Model.Song

import kien.test.downloadmusicfree.R
import kien.test.downloadmusicfree.ViewModel.SongViewModel
import java.io.File

/**
 * A simple [Fragment] subclass.
 */
class MusicUSUKFragment : BaseFragment(), MusicAdapter.CallBackDownLoad {

    lateinit var rvData: RecyclerView
    lateinit var adapterSongs: MusicAdapter
    lateinit var songs: MutableList<Song>
    lateinit var ct: Context
    lateinit var mModel: SongViewModel
    lateinit var prgLoading: ProgressBar
    private var Download_Uri: Uri? = null
    private var downloadManager: DownloadManager? = null

    override fun onClick(url: String, name: String) {
        val folder =
            File(Environment.DIRECTORY_DOWNLOADS + File.separator + getString(R.string.app_name))
        if (!folder.exists()) {
            folder.mkdirs()
        }
        DownloadFromUrl(url, name + ".mp3")
    }

    override fun attachLayout(): Int {
        return R.layout.fragment_music_usuk
    }

    override fun initView() {
        prgLoading = viewChild.findViewById(R.id.prgLoading)
        rvData = viewChild.findViewById(R.id.rvMusic)
        val manager: LinearLayoutManager = LinearLayoutManager(activity)
        songs = mutableListOf()
        adapterSongs = MusicAdapter(ct, songs, this)
        rvData.setHasFixedSize(true)
        rvData.layoutManager = manager
        rvData.adapter = adapterSongs

        downloadManager = activity!!.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
    }

    override fun initEven() {
        mModel = ViewModelProviders.of(this).get(SongViewModel::class.java)
        mModel.getListData(EnumType.US_UK.type).observe(this, Observer { data ->
            adapterSongs.setData(data)
            prgLoading.visibility = View.GONE
        })
    }

    override fun initDataServer() {
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDataServer()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            ct = context
        }
    }

    fun DownloadFromUrl(url: String, name: String) {
        Download_Uri = Uri.parse(url)
        val request = DownloadManager.Request(Download_Uri)
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setAllowedOverRoaming(false)
        request.setTitle(
            getString(R.string.title_down) + " " + name
        )
        request.setDescription(getString(R.string.Downloading))
        request.setVisibleInDownloadsUi(true)
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            File.separator + getString(R.string.app_name) + File.separator + name
        )

        downloadManager!!.enqueue(request)
    }
}
