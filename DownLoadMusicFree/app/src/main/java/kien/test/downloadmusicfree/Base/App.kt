package kien.test.downloadmusicfree.Base

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication


class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        mSelf = this
    }

    companion object {
        private var mSelf: App? = null

        fun self(): App? {
            return mSelf
        }
    }
}