package kien.test.downloadmusicfree.Model

data class Song(
    var nameSong: String,
    var nameArtis: String,
    var linkAvatar: String,
    var linkDownLoad: String
) {
    constructor() : this("", "", "", "")
}