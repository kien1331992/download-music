package kien.test.downloadmusicfree.Base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kien.test.downloadmusicfree.Interface.BaseInterface
import kien.test.downloadmusicfree.R

open abstract class BaseFragment : Fragment(), BaseInterface {

    lateinit var viewChild: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewChild = inflater.inflate(attachLayout(), container, false)
        // Inflate the layout for this fragment
        return viewChild
    }

    open override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initEven()
    }

    open abstract fun attachLayout(): Int
}