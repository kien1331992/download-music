package kien.test.downloadmusicfree.Base

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kien.test.downloadmusicfree.Interface.BaseInterface
import android.content.Intent



abstract class BaseActivity : AppCompatActivity(), BaseInterface, PermissionListener {
    companion object {
        val WRITE_CODE: Int = 300
    }

    open override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(attachLayout())
        checkPermission()
        initView()
        initEven()
    }

    fun checkPermission() {
        Dexter.withActivity(this)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(this)
            .check()
    }

    override fun onPermissionGranted(response: PermissionGrantedResponse) {
        // This method will be called when the permission is denied
    }

    override fun onPermissionRationaleShouldBeShown(
        permission: PermissionRequest,
        token: PermissionToken?
    ) {
        // This method will be called when the user rejects a permission request
        // You must display a dialog box that explains to the user why the application needs this permission
        if (token != null) {
            token.continuePermissionRequest()
        }
    }

    override fun onPermissionDenied(response: PermissionDeniedResponse) {
        // This method will be called when the permission is granted
        val homeIntent = Intent(Intent.ACTION_MAIN)
        homeIntent.addCategory(Intent.CATEGORY_HOME)
        homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(homeIntent)
    }

    abstract fun attachLayout(): Int
}