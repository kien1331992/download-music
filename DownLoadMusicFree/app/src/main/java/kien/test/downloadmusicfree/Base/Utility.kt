package kien.test.downloadmusicfree.Base

import android.app.Activity
import android.content.Context
import android.content.Intent

class Utility {
    companion object {
        inline fun <reified T : Activity> startActivity(ctxt: Context) {
            ctxt.startActivity(Intent(ctxt, T::class.java))
        }
    }
}