package kien.test.downloadmusicfree.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import kien.test.downloadmusicfree.Model.Song
import kien.test.downloadmusicfree.Service.SongRepository


class SongViewModel(application: Application) : AndroidViewModel(application) {
    lateinit var mutableLiveData: MutableLiveData<MutableList<Song>>
    lateinit var repository: SongRepository

    init {
        mutableLiveData = MutableLiveData<MutableList<Song>>()
        repository = SongRepository.getInstance()
    }

    fun getListData(typeNode: String): MutableLiveData<MutableList<Song>> {
        repository.getAllSong({ repos ->
            mutableLiveData.postValue(repos)
        }, typeNode)
        return mutableLiveData
    }

}